var firebaseConfig = {
    apiKey: "AIzaSyABpFLknFtX8JPqt_qy4i6EwZ10rYdoPE8",
    authDomain: "pc-store-7d95f.firebaseapp.com",
    projectId: "pc-store-7d95f",
    storageBucket: "pc-store-7d95f.appspot.com",
    messagingSenderId: "917361021566",
    appId: "1:917361021566:web:a2afc3308a5479e0a23e2f"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

//global
var productos= JSON.parse(localStorage.getItem('cart'));
var cartItems=[];
var cart_n = document.getElementById('cart_n');
var table= document.getElementById('table');
var total=0;
//HTML
function tableHTML(i){
    return`
            <tr>
            <th scope="row">${i+1}</th> 
            <td><img style="width:90px;" src="${productos[i].url}"></td>
            <td>${productos[i].name}</td> 
            <td>${productos[i].cantidad}</td>
            <td>${productos[i].price}</td>        
            </tr>
    `
    
}
function buy(){
    var d =new Date();
    var t = d.getTime();
    var counter=t;
    counter+=1;
    let db=firebase.database().ref("order/"+counter);
    let itemdb={
        id:counter,
        order:counter-895,
        total:total
    }
    db.set(itemdb);
    swal({
        position:'center',
        type:'success',
        tittle:'Purchase made successfully!',
        text:`Your purchase order is: ${itemdb.order}`,
        showConfirmButton:true,
        timer:50000
    });
    clean();
}
//clean
function clean(){
    localStorage.clear();
    for (let index = 0; index < productos.length; index++){
        table.innerHTML+=tableHTML(index);
        total=total+parseInt(productos[index].price);
    }
    total=0;
    table.innerHTML=`
    <tr>
    <th></th> 
    <th></th>
    <th></th> 
    <th></th>
    <th></th>
    </tr>
    `;
    cart_n.innerHTML='';
    document.getElementById("btnBuy").style.display="none";
    document.getElementById("btnClean").style.display="none";
}

function render(){
    for (let index = 0; index < productos.length; index++){
        table.innerHTML+=tableHTML(index);
        total=total+parseInt(productos[index].price);
    }
    table.innerHTML+=`
    <tr>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col">Total: $${total}.00</th>
    </tr>
    <tr>
    <th scope="col"></th>
    <th scope="col"></th>
    <th scope="col">
        <button id="btnClean" onclick="clean()" class"btn text-white
        btn-warning">clean shopping cart</button>
    </th>
    <th scope="col">
        <button id="btnBuy" onclick="buy()" class"btn text-white
        btn-success">Buy</button>
    </th>
    <th scope="col">Total: $${total}.00</th>
    </tr>
    `;
    productos=JSON.parse(localStorage.getItem("cart"));    
    cart_n.innerHTML=`[${productos.length}]`;
}



